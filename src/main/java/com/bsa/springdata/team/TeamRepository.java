package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    Optional<Team> findByName(String teamName);

    int countByTechnologyName(String technology);


    //    @Query( value = "UPDATE technologies SET name = :newTechnologyName " +
//                    "WHERE technologies.name = :oldTechnologyName  and technologies.id in " +
//            "(Select teams.technology_id  " +
//            "from teams, users where teams.id = users.team_id " +
//            "group by teams.technology_id " +
//            "having count(users) < :devNumber ) "
//            , nativeQuery = true)
    //         "AND t.users.size < :devNumber ")
    @Modifying
    @Transactional
    @Query(value = " UPDATE teams SET technology_id = " +
            "(SELECT tech.id FROM technologies tech WHERE tech.name=:newTechnologyName)" +
            "WHERE teams.technology_id IN (SELECT tech.id " +
            "FROM technologies tech , teams t, users u " +
            "WHERE tech.name=:oldTechnologyName " +
            "AND t.id = u.team_id " +
            "AND tech.id = t.technology_id " +
            "GROUP BY  tech.id " +
            "HAVING count(u) > :devsNumber)", nativeQuery = true)
    void updateTechnology(int devsNumber,
                          String oldTechnologyName,
                          String newTechnologyName);


    @Modifying
    @Transactional
    @Query(value = "UPDATE teams SET name = " +
            "(SELECT CONCAT (t.name ,'_', p.name , '_', tech.name) " +
            "FROM projects p, technologies tech, teams t " +
            "WHERE t.project_id = p.id " +
            "AND t.technology_id = tech.id " +
            "AND t.name = :teamName)" +
            "WHERE teams.name = :teamName", nativeQuery = true)
    void normalizeName(String teamName);
}

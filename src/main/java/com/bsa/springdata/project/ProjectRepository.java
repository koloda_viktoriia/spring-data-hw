package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query("SELECT p FROM Project p " +
            "LEFT JOIN  p.teams t  " +
            "WHERE :technology = ANY(SELECT t.technology.name FROM p.teams t)" +
            "GROUP BY p" +
            " ORDER BY SUM(t.users.size)")
    List<Project> findTop5ByTechnology(String technology, Pageable pageable);

    @Query("SELECT p FROM Project p " +
            "LEFT JOIN  p.teams t  " +
            "GROUP BY p" +
            " ORDER BY p.teams.size DESC , SUM(t.users.size) DESC, p.name DESC")
    List<Project> findTheBiggest(Pageable pageable);

    @Query(value = "SELECT DISTINCT p.name as name  , COUNT(distinct t) as teamsNumber, " +
            "COUNT(distinct  u) as developersNumber, " +
            "STRING_AGG( DISTINCT tech.name, ',' ORDER BY tech.name DESC) as technologies " +
            "FROM teams t, projects p, users u, technologies tech " +
            "WHERE t.project_id=p.id AND  t.id = u.team_id " +
            "AND t.technology_id = tech.id " +
            "GROUP BY p.name", nativeQuery = true)
    List<ProjectSummaryDto> getSummary();

    @Query(value = "SELECT COUNT( distinct p)" +
            "FROM projects p, teams t, users u , user2role ur , roles r " +
            "WHERE r.name=:role " +
            "AND p.id = t.project_id " +
            "AND t.id=u.team_id " +
            "AND u.id = ur.user_id " +
            "AND r.id = ur.role_id", nativeQuery = true)
    int getCountWithRole(String role);
}
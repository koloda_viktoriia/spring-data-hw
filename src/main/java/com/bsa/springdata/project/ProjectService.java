package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository.findTop5ByTechnology(technology, PageRequest.of(0, 5))
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {

        var project = Optional.of(projectRepository.findTheBiggest(PageRequest.of(0, 1)))
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList())
                .get(0);
        return Optional.of(project);
    }

    public List<ProjectSummaryDto> getSummary() {

        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {

        return projectRepository.getCountWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        Technology technology = Technology.builder()
                .name(createProjectRequest.getTech())
                .description(createProjectRequest.getTechDescription())
                .build();
        technologyRepository.save(technology);

        Project project = Project.builder()
                .name(createProjectRequest.getProjectName())
                .description(createProjectRequest.getProjectDescription())
                .build();
        UUID projectId = projectRepository.save(project).getId();

        Team team = Team.builder()
                .room(createProjectRequest.getTeamRoom())
                .name(createProjectRequest.getTeamName())
                .area(createProjectRequest.getTeamArea())
                .project(project)
                .technology(technology)
                .build();
        teamRepository.save(team);

        return projectId;
    }
}

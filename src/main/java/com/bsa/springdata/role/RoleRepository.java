package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Query("DELETE  FROM Role r  WHERE r.code = :code  AND  r.users IS EMPTY ")
    @Modifying
    @Transactional
    int deleteRoleByCode(String code);

}

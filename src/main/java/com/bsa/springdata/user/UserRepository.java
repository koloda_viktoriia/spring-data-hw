package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {


    List<User> findAllByLastNameLikeIgnoreCase(String lastName, Pageable pageable);

   @Query("SELECT u FROM User u WHERE u.office.city = :city ORDER BY u.lastName ASC")
    List<User> findALLByCity(String city);

    List<User> findALLByExperienceGreaterThanEqual(int experience, Sort sort);

    @Query("SELECT u FROM User u WHERE u.office.city=:city AND u.team.room=:room")
    List<User> findALLByCityAndRoom(String city, String room, Sort sort);

    int deleteAllByExperienceLessThan(int experience);
}

package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query(value = "SELECT DISTINCT o.id , o.city , o.address " +
            "FROM offices o , technologies tech , users u , teams t " +
            "WHERE tech.name = :technology " +
            "AND o.id = u.office_id " +
            "AND t.id = u.team_id " +
            "AND tech.id = t.technology_id  ", nativeQuery = true)
    List<Office> getByTechnology(String technology);

    @Modifying
    @Transactional
    @Query(value = "UPDATE  offices SET address =:newAddress " +
            "WHERE address IN " +
            "(SELECT o.address " +
            "FROM users u, teams t , offices o " +
            "WHERE  o.address = :oldAddress " +
            "AND o.id =u.office_id " +
            "AND t.id = u.team_id " +
            "AND t.project_id IS NOT NULL)", nativeQuery = true)
    void updateAddress(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String newAddress);
}
